/**
 * Created by anton on 22.04.17.
 */
import java.io.*;


public class MainClass {


    private final static String KEY_WORD = "onetwotrip";
    private final static String WRONG_RESULT = "Impossible";
    private final static String INPUT_FILE_NAME = "input.txt";
    private final static String OUTPUT_FILE_NAME = "output.txt";


    public static void main(String[] args){

        Word[] mResult = new Word[KEY_WORD.length()];
        char[] mKeyWord = KEY_WORD.toCharArray();
        int m, n;
        File mInputFile = new File( INPUT_FILE_NAME);

        try (BufferedReader mBufferReader = new BufferedReader(new FileReader(mInputFile))) {
            String mLine = mBufferReader.readLine();

            int mSeparator = mLine.indexOf(' ');

            m = new Integer(mLine.substring(0, mSeparator));
            n = new Integer(mLine.substring(mSeparator+1));




            int lineNumber = 0;
            int charCounter = 0;

            char currentChar;


            while ((mLine = mBufferReader.readLine()) != null) {

                mLine = mLine.replaceAll("\\s+", "");

                for (int i = 0; i < n; ++i){
                    currentChar = mLine.charAt(i);

                    for (int j = 0; j < KEY_WORD.length(); ++j) {
                        if (mKeyWord[j] == Character.toLowerCase(currentChar)) {
                            mResult[j] = new Word(currentChar, lineNumber, i);
                            mKeyWord[j] = '\n';
                            ++charCounter;
                            break;
                        }
                    }
                }
                if (charCounter == KEY_WORD.length()){
                    StringBuilder mStringBuilder = new StringBuilder();

                    for (int i = 0; i < KEY_WORD.length(); i++){
                        mStringBuilder.append(String.format("%s - (%d, %d);\n", mResult[i].getWord(), mResult[i].getNumberLine(), mResult[i].getNumberColumn()));
                    }

                    writeResult(mStringBuilder.toString());
                    return;
                }
                ++lineNumber;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        writeResult(WRONG_RESULT);
    }

    private static void writeResult(String result){

        File output = new File(OUTPUT_FILE_NAME);

        try(FileOutputStream os = new FileOutputStream(output)) {
            os.write(result.getBytes());
            os.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
