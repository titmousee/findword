/**
 * Created by anton on 22.04.17.
 */

public class Word {
    char word;
    int numberLine;
    int numberColumn;

    Word(char word, int numberLine, int numberColumn) {
        this.word = word;
        this.numberLine = numberLine;
        this.numberColumn = numberColumn;
    }

    char getWord() {
        return word;
    }

    int getNumberLine() {
        return numberLine;
    }

    int getNumberColumn() {
        return numberColumn;
    }
}
